package com.portalservice.Portal;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface UserTransactionRepository extends JpaRepository<UserTransactionEntity, Integer> {

	
	List<UserTransactionEntity> findTransactionsByPlayerId(int playerId);
	
	List<UserTransactionEntity> findTopByPlayerIdOrderByIdDesc(int playerId);
	
	UserTransactionEntity findFirstByOrderByCreatedDateDesc();
}
