package com.portalservice.Portal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserTransactionService {

	public static final String FREE = "Free";

	@Autowired
	UserTransactionRepository userTxRepo;


	public List<UserTransactionEntity> getAllUserTransactions() {
		List<UserTransactionEntity> allTransactions = userTxRepo.findAll();

		if (allTransactions.size() > 0) {
			return allTransactions;
		} else {
			return new ArrayList<UserTransactionEntity>();
		}
	}

	public UserTransactionEntity getTxDataById(int id) throws Exception {
		Optional<UserTransactionEntity> userTx = userTxRepo.findById(id);

		if (userTx.isPresent()) {
			return userTx.get();
		} else {
			throw new Exception("Not found");
		}
	}

	public List<UserTransactionEntity> getTxDataByUserId(int playerId) throws Exception {
		List<UserTransactionEntity> userTx = userTxRepo.findTransactionsByPlayerId(playerId);

		if (!userTx.isEmpty()) {
			return userTx;
		} else {
			throw new Exception("Not found");
		}
	}

	public UserTransactionEntity createOrUpdateTransaction(UserTransactionEntity entity) throws Exception {
		int id = entity.getTxId();
		Date date = new Date(System.currentTimeMillis());
		Optional<UserTransactionEntity> userTx = userTxRepo.findById(entity.getTxId());

		if (id != 0 && userTx.isPresent()) {
			UserTransactionEntity newEntity = userTx.get();
			newEntity.setTxType(entity.getTxType());
			newEntity.setIsTxFree(entity.getIsTxFree());
			newEntity.setAmount(entity.getAmount());
			newEntity.setModifiedDate(date);
			newEntity.setPlayerId(entity.getPlayerId());
			newEntity = userTxRepo.save(newEntity);
			return newEntity;
		} else {
			entity.setCreatedDate(date);
			entity = userTxRepo.save(entity);
			return entity;
		}
	}

	public UserTransactionEntity createTransaction(UserDataEntity userEntity, int betAmount, String txType)
			throws Exception {
		UserTransactionEntity txEntity = new UserTransactionEntity();
		txEntity.setTxType(txType);
		Date date = new Date(System.currentTimeMillis());
		List<UserTransactionEntity> lastTxEntry = userTxRepo.findTopByPlayerIdOrderByIdDesc(userEntity.getId());
		int wonAmount = getWiningAmount(betAmount);
		
		if (FREE.equals(txType)) {
			System.out.println("You could have won/lost : "+ wonAmount);
			System.out.println("Would you like to place a bet now ?");
		} else {
			txEntity.setAmount(wonAmount);
			if(lastTxEntry != null && !lastTxEntry.isEmpty() && lastTxEntry.get(lastTxEntry.size()-1).getIsTxFree()==true) {
				if(wonAmount > 0)
				userEntity.setBalance(wonAmount + userEntity.getBalance());
			} else {
				userEntity.setBalance(wonAmount + userEntity.getBalance());
			}
		}
		txEntity.setIsTxFree(checkIsTxFree());
		txEntity.setCreatedDate(date);
		txEntity.setModifiedDate(date);
		txEntity.setPlayerId(userEntity.getId());
		txEntity = userTxRepo.save(txEntity);
		return txEntity;
	}

	private boolean checkIsTxFree() {
		return Math.random() * 100 >89 ? true : false;
	}

	private int simpleCalculationWinning(int betAmount) {
		return Math.random() * 100 > 69 ? 20 : -betAmount;
	}

	private int getWiningAmount(int betAmount) {
		if (betAmount < 5) {
			return randomWithRange(0, 40) >= 30 ? 20 + betAmount : -betAmount;
		} else if (betAmount < 8) {
			return randomWithRange(11, 50) >= 30 ? 20 + betAmount: -betAmount;
		} else {
			return randomWithRange(21, 70) >= 30 ? 20 + betAmount: -betAmount;
		}
	}

	private int randomWithRange(int min, int max) {
		int range = (max - min) + 1;
		return (int) (Math.random() * range) + min;
	}

	public void deleteTransactionById(int id) throws Exception {
		Optional<UserTransactionEntity> userTx = userTxRepo.findById(id);

		if (userTx.isPresent()) {
			userTxRepo.deleteById(id);
		} else {
			throw new Exception("Not found");
		}
	}
}
