package com.portalservice.Portal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="player_data")
public class UserDataEntity {
	@Column(name="PlayerName")
	private String name;
	@Column(name="Address")
	private String address;
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="PlayerId")
	private int id;
	

	@Column(name="Phone")
	private long phone;
	@Column(name="Balance")
	private int balance;
	@Column(name="CreatedDate")
	private Date createdDate;
	@Column(name="CreatedDate", insertable = false, updatable = false)
	private Date ModifiedDate;
	private Date modifiedDate;
	@Column(name="DOB")
	private Date dob;

	public UserDataEntity() {
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public long getPhone() {
		return this.phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Date getDOB() {
		return this.dob;
	}

	public void setDOB(Date dob) {
		this.dob = dob;
	}

}