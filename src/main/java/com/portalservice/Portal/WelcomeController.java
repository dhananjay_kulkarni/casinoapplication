package com.portalservice.Portal;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class WelcomeController extends DBParser{
	
	
    @Autowired
    UserDataService userDataService;
    
    @Autowired
    UserTransactionService userTxService;
	
	public WelcomeController() {
	}

	@GetMapping("/")
	public String welcome() {
		return "Welcome to the Casino service!";
	}

	
    @GetMapping("/user={userid}")
    public ResponseEntity<UserDataEntity> getUserDataById(@PathVariable("userid") int id) 
                                                    throws Exception {
    	UserDataEntity entity = userDataService.getUserDataById(id);
 
        return new ResponseEntity<UserDataEntity>(entity, new HttpHeaders(), HttpStatus.OK);
    }
	
    @PostMapping("/createOrUpdateUser")
    public ResponseEntity<UserDataEntity> createOrUpdateUser(@RequestBody UserDataEntity user)
                                                    throws Exception {
    	UserDataEntity updatedUser = userDataService.createOrUpdateUser(user);
        return new ResponseEntity<UserDataEntity>(updatedUser, new HttpHeaders(), HttpStatus.OK);
    }
    
    @DeleteMapping("/user={userid}")
    public HttpStatus deleteEmployeeById(@PathVariable("userid") int id) 
                                                    throws Exception {
    	userDataService.deleteUserById(id);
        return HttpStatus.FORBIDDEN;
    }

    @GetMapping("/tx={txid}")
    public ResponseEntity<UserTransactionEntity> getTxDataById(@PathVariable("txid") int id) 
                                                    throws Exception {
    	UserTransactionEntity entity = userTxService.getTxDataById(id);
 
        return new ResponseEntity<UserTransactionEntity>(entity, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping("/createOrUpdateTx")
    public ResponseEntity<UserTransactionEntity> createOrUpdateTx(@RequestBody UserTransactionEntity userTx)
                                                    throws Exception {
    	UserTransactionEntity updatedUserTx = userTxService.createOrUpdateTransaction(userTx);
        return new ResponseEntity<UserTransactionEntity>(updatedUserTx, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping("/placeBet")
    public ResponseEntity<UserTransactionEntity> placeBet(@RequestBody UserTransactionEntity userTx)
                                                    throws Exception {
    	
    	UserDataEntity entity = userDataService.getUserDataById(userTx.getPlayerId());
    	UserTransactionEntity updatedUserTx = userTxService.createTransaction(entity, userTx.getAmount(), userTx.getTxType() );
    	
    	
        return new ResponseEntity<UserTransactionEntity>(updatedUserTx, new HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping("/playerId={playerId}")
    public ResponseEntity<List<UserTransactionEntity>> getTxDataByPlayerId(@PathVariable("playerId") int id) 
                                                    throws Exception {
    	List<UserTransactionEntity> entity = userTxService.getTxDataByUserId(id);
 
        return new ResponseEntity<List<UserTransactionEntity>>(entity, new HttpHeaders(), HttpStatus.OK);
    }
 
    @DeleteMapping("/tx={txid}")
    public HttpStatus deleteTxById(@PathVariable("txid") int id) 
                                                    throws Exception {
    	userTxService.deleteTransactionById(id);
        return HttpStatus.FORBIDDEN;
    }
	
	@GetMapping("/welcome")
	public String welcome1() {
		return "Welcome to the portal service!";
	}

}