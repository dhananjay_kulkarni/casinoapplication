package com.portalservice.Portal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserDataService {

	@Autowired
	private UserDataRepository userDataRepo;

	public List<UserDataEntity> getAllUserTransactions() {
		List<UserDataEntity> allUsers = userDataRepo.findAll();

		if (allUsers.size() > 0) {
			return allUsers;
		} else {
			return new ArrayList<UserDataEntity>();
		}
	}

	public UserDataEntity getUserDataById(int id) throws Exception {
		Optional<UserDataEntity> userTx = userDataRepo.findById(id);

		if (userTx.isPresent()) {
			return userTx.get();
		} else {
			throw new Exception("Not found");
		}
	}

	public UserDataEntity createOrUpdateUser(UserDataEntity entity) throws Exception {
		int id = entity.getId();
		Date date = new Date(System.currentTimeMillis());
		Optional<UserDataEntity> user = userDataRepo.findById(id);
		if (id != 0 && user.isPresent()) {
			UserDataEntity newEntity = user.get();
			newEntity.setName(entity.getName());
			newEntity.setAddress(entity.getAddress());
			newEntity.setBalance(entity.getBalance());
			newEntity.setModifiedDate(date);
			newEntity.setPhone(entity.getPhone());
			newEntity.setDOB(entity.getDOB());
			newEntity = userDataRepo.save(newEntity);
			return newEntity;
		} else {
			entity.setCreatedDate(date);
			entity = userDataRepo.save(entity);
			return entity;
		}
	}

	public void deleteUserById(int id) throws Exception {
		Optional<UserDataEntity> user = userDataRepo.findById(id);

		if (user.isPresent()) {
			userDataRepo.deleteById(id);
		} else {
			throw new Exception("Not found");
		}
	}
}
