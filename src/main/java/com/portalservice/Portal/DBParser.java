package com.portalservice.Portal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBParser {
	private static Connection dbConnection = null;

	private static final String URL = "jdbc:mysql://evolution-db.mysql.database.azure.com:3306/evolution?useSSL=true&requireSSL=false";
	private static final String USER = "user1@evolution-db";
	private static final String PASSWORD = "Admin@123";

	public static int getConnection() {
		if (dbConnection == null) {
			return initializeDbConnection();
		}
		return 0;
	}

	public static UserDataEntity getUserData(int id) throws SQLException {

		Statement stmt = dbConnection.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM playerdata WHERE PlayerId=" + id);
		if (rs.next()) {
			UserDataEntity user = new UserDataEntity();
			user.setId(rs.getInt("PlayerId"));
			user.setName(rs.getString("PlayerName"));
			user.setBalance(rs.getInt("Balance"));
			user.setAddress(rs.getString("Address"));
			user.setPhone(rs.getLong("Phone"));
			user.setDOB(rs.getDate("DOB"));
//			user.setCreatedDate(rs.getDate("CreatedDate"));
			user.setModifiedDate(rs.getDate("ModifiedDate"));
			return user;
		} 
		return null;
	}

	private static void createUser() {
		// TODO Auto-generated method stub
		
	}

	public static int setUserData() {
		return 0;
	}

	public static UserTransactionEntity getTxData(int id) throws SQLException {
		
		Statement stmt = dbConnection.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM transactiondata INNER JOIN playerdata ON transactiondata.PlayerId = playerdata.PlayerId WHERE PlayerId=" + id);
		if (rs.next()) {
			UserTransactionEntity currentTx = new UserTransactionEntity();
			currentTx.setTxId(rs.getInt("TransactionId"));
//			currentTx.setId(rs.getInt("PlayerId"));
			currentTx.setAmount(rs.getInt("Amount"));
			currentTx.setTxType(rs.getString("TransType"));
			currentTx.setIsTxFree(rs.getBoolean("isTxFree"));
//			currentTx.setCreatedDate(rs.getDate("CreatedDate"));
			currentTx.setModifiedDate(rs.getDate("ModifiedDate"));
			return currentTx;
		}
		return null;
	}

	public static int setTxData() {
		return 0;
	}

	public static int initializeDbConnection() {
		try {
			dbConnection = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return -1;
		}
		finally {
			try {
				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
		 }
		return 0;

	}
}