package com.portalservice.Portal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="transaction_data")
public class UserTransactionEntity {
	
	@Column(name="TransType")
	private String txType;
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="TransId")
	private int id;
	@Column(name="Amount")
	private int amount;
	@Column(name="CreatedDate", insertable = false, updatable = false)
	private Date createdDate;
	@Column(name="ModifiedDate")
	private Date modifiedDate;
	@Column(name="isTxFree")
	private boolean isTxFree;
	@Column(name="PlayerId")
	private int playerId;
	

	

	public int getPlayerId() {
		return playerId;
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	public UserTransactionEntity() {
	}

	public String getTxType() {
		return txType;
	}

	public void setTxType(String txType) {
		this.txType = txType;
	}

	public boolean getIsTxFree() {
		return isTxFree;
	}

	public void setIsTxFree(boolean isTxFree) {
		this.isTxFree = isTxFree;
	}

	public int getTxId() {
		return this.id;
	}
	
	public void setTxId(int txId) {
		this.id = txId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int balance) {
		this.amount = balance;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
}